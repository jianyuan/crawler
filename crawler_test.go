package crawler

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"golang.org/x/net/html/atom"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

// newTestSite creates a test web site.
func newTestSite() *httptest.Server {
	mux := http.NewServeMux()

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/" {
			http.NotFound(w, r)
			return
		}

		fmt.Fprint(w, `
<title>Hello</title>
<a href="/">Self</a>
<a href="/status/200">200</a>
<a href="/redirect?url=/status/200">redirect to 200</a>
<a href="/status/404">404</a>
<a href="/links">links</a>
<a href="http://google.com/">Google.com (external)</a>
`)
	})

	mux.HandleFunc("/status/200", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

	mux.HandleFunc("/redirect", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, r.URL.Query().Get("url"), http.StatusTemporaryRedirect)
	})

	mux.HandleFunc("/links", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, `
<title>Links</title>
<a href="/">Home</a>
<a href="/images">Images</a>
`)
	})

	mux.HandleFunc("/images", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, `
<title>Images</title>
<a href="/">Home</a>
<img src="/image.jpg">
`)
	})

	return httptest.NewServer(mux)
}

func TestCrawler(t *testing.T) {
	ts := newTestSite()
	defer ts.Close()

	base, _ := url.Parse(ts.URL)

	c := New()
	c.ShouldVisitFunc = ShouldVisitSameHost(base)

	res, err := c.Run(base)
	require.NoError(t, err)

	// Check page URLs
	expectedPageURLs := []url.URL{
		*base.ResolveReference(&url.URL{}),
		*base.ResolveReference(&url.URL{Path: "/"}),
		*base.ResolveReference(&url.URL{Path: "/images"}),
		*base.ResolveReference(&url.URL{Path: "/links"}),
		*base.ResolveReference(&url.URL{Path: "/redirect", RawQuery: "url=/status/200"}),
		*base.ResolveReference(&url.URL{Path: "/status/200"}),
		*base.ResolveReference(&url.URL{Path: "/status/404"}),
	}
	actualPageURLs := make([]url.URL, 0, len(res.Pages))
	for _, page := range res.Pages {
		actualPageURLs = append(actualPageURLs, *page.URL)
	}
	assert.Equal(t, expectedPageURLs, actualPageURLs)

	// Check page titles
	expectedPageTitles := map[string]string{
		"":        "Hello",
		"/":       "Hello",
		"/links":  "Links",
		"/images": "Images",
	}
	for _, page := range res.Pages {
		if expectedPageTitle, ok := expectedPageTitles[page.URL.Path]; ok {
			assert.Equal(t, expectedPageTitle, page.Title, "Path: %q", page.URL.Path)
		} else {
			assert.Empty(t, page.Title)
		}
	}

	// Check HTTP statuses
	expectedHTTPStatuses := map[string]int{
		"":            http.StatusOK,
		"/":           http.StatusOK,
		"/links":      http.StatusOK,
		"/images":     http.StatusOK,
		"/redirect":   http.StatusOK,
		"/status/200": http.StatusOK,
		"/status/404": http.StatusNotFound,
	}
	for _, page := range res.Pages {
		if expectedHTTPStatus, ok := expectedHTTPStatuses[page.URL.Path]; ok {
			assert.Equal(t, expectedHTTPStatus, page.StatusCode, "Path: %q", page.URL.Path)
		} else {
			assert.Fail(t, fmt.Sprintf("HTTP status code for %q not defined", page.URL.Path))
		}
	}

	// Check images
	expectedImages := map[string][]url.URL{
		"/images": []url.URL{
			*base.ResolveReference(&url.URL{Path: "/image.jpg"}),
		},
	}
	for _, page := range res.Pages {
		if expectedImages, ok := expectedImages[page.URL.Path]; ok {
			actualImages := make([]url.URL, 0, len(page.Assets[atom.Img]))
			for _, page := range page.Assets[atom.Img] {
				actualImages = append(actualImages, *page.URL)
			}
			assert.Equal(t, expectedImages, actualImages)
		}
	}

	t.Logf("Pages: %s", res.Pages)
}
