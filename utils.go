package crawler

import (
	"net/http"
	"net/url"
)

// NetHTTPGet is a wrapper around net/http.Get method
func NetHTTPGet(ref *url.URL) (*http.Response, error) {
	return http.Get(ref.String())
}

// ShouldVisitSameHost returns a predicate function that checks if the host of
// the base URL matches that of the passed URL.
func ShouldVisitSameHost(base *url.URL) func(*url.URL) bool {
	return func(ref *url.URL) bool {
		return ref.Host == base.Host
	}
}

// DefaultURINormaliser normalises URI for history tracking.
func DefaultURINormaliser(ref *url.URL) *url.URL {
	if ref != nil {
		// Blankify URI fragment
		ref.Fragment = ""
	}
	return ref
}
