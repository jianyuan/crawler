package crawler

import (
	"fmt"
	"net/url"
	"sort"
	"strings"

	"golang.org/x/net/html/atom"
)

// Asset describes an asset on a page.
type Asset struct {
	URL     *url.URL
	Visited bool
}

// AssetSlice attaches the methods of sort.Interface to []*Page,
// sorting in increasing order.
type AssetSlice []*Asset

func (p AssetSlice) Len() int           { return len(p) }
func (p AssetSlice) Less(i, j int) bool { return p[i].URL.String() < p[j].URL.String() }
func (p AssetSlice) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

// Sort is a convenience method.
func (p AssetSlice) Sort() { sort.Sort(p) }

// SortAssets sorts a slice of Pages in increasing order.
func SortAssets(p []*Asset) { sort.Sort(AssetSlice(p)) }

// AssetMap groups a slice of asset URLs by HTML element type.
type AssetMap map[atom.Atom][]*Asset

// String summarises AssetMap by counting the number of assets for each
// HTML element type.
func (am AssetMap) String() string {
	texts := make([]string, 0, len(am))
	for kind, assets := range am {
		texts = append(texts, fmt.Sprintf("%d <%s /> asset(s)", len(assets), kind))
	}
	return strings.Join(texts, "; ")
}

// Page contains information about a crawl result.
type Page struct {
	URL         *url.URL
	Err         error
	StatusCode  int
	Status      string
	ContentType string
	Title       string
	Assets      AssetMap
}

func (p Page) String() string {
	if p.Err != nil {
		return fmt.Sprintf("Page URL: %s, Error: %s", p.URL, p.Err)
	}
	return fmt.Sprintf(
		"Page URL: %s, Status: %s, Content Type: %s, Title: %s, Assets: %s",
		p.URL, p.Status, p.ContentType, p.Title, p.Assets,
	)
}

// PageSlice attaches the methods of sort.Interface to []*Page,
// sorting in increasing order.
type PageSlice []*Page

func (p PageSlice) Len() int           { return len(p) }
func (p PageSlice) Less(i, j int) bool { return p[i].URL.String() < p[j].URL.String() }
func (p PageSlice) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

// Sort is a convenience method.
func (p PageSlice) Sort() { sort.Sort(p) }

// SortPages sorts a slice of Pages in increasing order.
func SortPages(p []*Page) { sort.Sort(PageSlice(p)) }
