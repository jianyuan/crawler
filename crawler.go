package crawler

import (
	"fmt"
	"net/http"
	"net/url"
	"os"
	"sync"
	"time"

	"golang.org/x/net/html/atom"

	log "github.com/Sirupsen/logrus"
	"gopkg.in/tomb.v2"
)

func init() {
	// Log to stderr
	log.SetOutput(os.Stderr)
}

// Crawler holds configuration and orchestrates the web crawler.
type Crawler struct {
	t tomb.Tomb

	// HTTPGetFunc issues a GET method HTTP request to the URL.
	HTTPGetFunc func(ref *url.URL) (*http.Response, error)

	// ShouldVisitFunc determines whether the crawler should visit the URL.
	// Leave nil to crawl all pages.
	ShouldVisitFunc func(ref *url.URL) bool

	History   *History
	Extractor *Extractor
}

// New initialises an instance of Crawler with sensible defaults.
func New() *Crawler {
	return &Crawler{
		HTTPGetFunc: NetHTTPGet,
		History:     NewHistory(),
		Extractor:   NewExtractor(),
	}
}

// Run initiates the web crawler.
func (c *Crawler) Run(base *url.URL) (*CrawlResult, error) {
	res, done := NewCrawlResult(base)
	defer done()

	c.goCrawl(base, res)

	if err := c.t.Wait(); err != nil {
		return nil, err
	}

	return res, nil
}

func (c *Crawler) goCrawl(ref *url.URL, res *CrawlResult) {
	c.t.Go(func() error {
		c.crawl(ref, res)
		return nil
	})
}

func (c *Crawler) crawl(ref *url.URL, res *CrawlResult) {
	log := log.WithField("url", ref)

	if c.History.Visited(ref) {
		log.Debug("[Crawler] Already visited")
		return
	}
	c.History.Add(ref)

	page := &Page{URL: ref}
	// Ensures page result is always added
	defer func() { res.AddPage(page) }()

	// Visit page
	log.Info("[Crawler] Visiting")
	resp, err := c.HTTPGetFunc(ref)
	if err != nil {
		page.Err = err
		log.Errorf("[Crawler] HTTP client error: %s", page.Err)
		return
	}

	// Copy HTTP response details
	page.StatusCode = resp.StatusCode
	page.Status = resp.Status
	page.ContentType = resp.Header.Get("Content-Type")

	if resp.StatusCode != http.StatusOK {
		page.Err = fmt.Errorf("Status code: %d (%s)", resp.StatusCode, resp.Status)
		log.Errorf("[Crawler] HTTP response error: %s", page.Err)
		return
	}

	defer resp.Body.Close()

	// Process page information
	title, assets, err := c.Extractor.Do(resp.Body, ref)
	if err != nil {
		log.Errorf("[Crawler] Page parsing error: %s", err)
		page.Err = err
		return
	}
	page.Title = title
	page.Assets = assets
	log.Debugf("[Crawler] Assets: %s", assets)

	// Crawl child links
	for _, childAsset := range assets[atom.A] {
		if c.ShouldVisitFunc == nil || c.ShouldVisitFunc(childAsset.URL) {
			c.goCrawl(childAsset.URL, res)
		}
	}
}

// CrawlResult contains the result of a crawl action.
type CrawlResult struct {
	BaseURL *url.URL

	pagesMu sync.Mutex
	Pages   []*Page

	RunTime   time.Duration
	startTime time.Time
}

// NewCrawlResult initialises a new instance of CrawlResult and a function
// to be executed once the result is done.
func NewCrawlResult(base *url.URL) (res *CrawlResult, done func()) {
	res = &CrawlResult{
		BaseURL:   base,
		startTime: time.Now(),
	}
	return res, res.Done
}

// AddPage adds a Page to the crawl result.
func (r *CrawlResult) AddPage(page *Page) {
	r.pagesMu.Lock()
	defer r.pagesMu.Unlock()
	r.Pages = append(r.Pages, page)
}

// Done post-processes the crawl results.
func (r *CrawlResult) Done() {
	// Determine which assets have been visited
	visited := make(map[url.URL]struct{}, len(r.Pages))
	for _, page := range r.Pages {
		visited[*page.URL] = struct{}{}
	}

	for _, page := range r.Pages {
		for _, assets := range page.Assets {
			for _, asset := range assets {
				_, ok := visited[*asset.URL]
				asset.Visited = ok
			}

			SortAssets(assets)
		}
	}

	SortPages(r.Pages)

	r.RunTime = time.Since(r.startTime)
}
