# crawler

`crawler` is a command line application that crawls a website.

## Requirements

- Go 1.6

## Installation

	go get -u bitbucket.org/jianyuan/crawler/cmd/crawler

## Usage

```
NAME:
   crawler - Crawls a website

USAGE:
   crawler [global options] command [command options] [arguments...]

VERSION:
   0.0.0

COMMANDS:
     sitemap  Generates a sitemap
     webui    Launches a Web UI after crawling
     help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --url value, -u value  url of website to crawl [$CRAWL_URL]
   --debug                enables debugging information [$DEBUG]
   --help, -h             show help
   --version, -v          print the version
```

There are two crawl result rendering options to choose from:

### Crawl and print sitemap to console

#### Example

```bash
$ crawler --url "http://example.com" sitemap

- Base URL: http://example.com

* URL: http://example.com
  Status: 200 OK
  Content Type: text/html
  Title: Example Domain
  <a />:
  - http://example.com
  - http://www.iana.org/domains/example

- Crawling and processing time: 77.648681ms
```

### Crawl and launch result server

#### Usage

```
NAME:
   crawler webui - Launches a Web UI after crawling

USAGE:
   crawler webui [command options] [arguments...]

OPTIONS:
   --bind value, -b value  HTTP binding address (default: "0.0.0.0:8080") [$HTTP_BIND]
```

#### Example

```bash
$ crawler --url "https://httpbin.org/" webui
# Logs omitted...
[INFO] About to listen on 0.0.0.0:8080
```

Now, open a browser and visit http://0.0.0.0:8080.

![Sitemap](_docs/sitemap.png)
![Page details](_docs/details.png)
