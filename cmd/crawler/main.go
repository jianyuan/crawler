package main

import (
	"fmt"
	"net/http"
	"net/url"
	"os"

	"bitbucket.org/jianyuan/crawler"
	log "github.com/Sirupsen/logrus"
	"github.com/urfave/cli"
)

func runCrawler(iref string) (*crawler.CrawlResult, error) {
	ref, err := url.Parse(iref)
	if err != nil {
		return nil, err
	}

	c := crawler.New()
	c.ShouldVisitFunc = crawler.ShouldVisitSameHost(ref)
	return c.Run(ref)
}

func main() {
	app := cli.NewApp()
	app.Name = "crawler"
	app.Usage = "Crawls a website"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "url, u",
			Usage:  "url of website to crawl",
			EnvVar: "CRAWL_URL",
		},
		cli.BoolFlag{
			Name:   "debug",
			Usage:  "enables debugging information",
			EnvVar: "DEBUG",
		},
	}
	app.Before = func(ctx *cli.Context) error {
		// Validate URL
		ref := ctx.String("url")

		if ref == "" {
			return cli.NewExitError("Please provide an intitial URL to crawl", 1)
		}

		_, err := url.Parse(ref)
		if err != nil {
			return cli.NewExitError(fmt.Sprintf("Invalid URL: %v", err), 1)
		}

		if _, err := http.Head(ref); err != nil {
			return cli.NewExitError(fmt.Sprintf("Cannot access URL: %v", err), 1)
		}

		ctx.GlobalSet("url", ref)

		// Handle debug flag
		if ctx.Bool("debug") {
			log.SetLevel(log.DebugLevel)
		}

		return nil
	}

	app.Commands = []cli.Command{
		{
			Name:  "sitemap",
			Usage: "Generates a sitemap",
			Action: func(ctx *cli.Context) error {
				res, err := runCrawler(ctx.GlobalString("url"))
				if err != nil {
					return cli.NewExitError(fmt.Sprintf("Crawl error: %v", err), 1)
				}

				ui := crawler.NewConsoleUI(res)
				ui.RenderTo(os.Stdout)

				return nil
			},
		},
		{
			Name:  "webui",
			Usage: "Launches a Web UI after crawling",
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:   "bind, b",
					Usage:  "HTTP binding address",
					EnvVar: "HTTP_BIND",
					Value:  "0.0.0.0:8080",
				},
			},
			Action: func(ctx *cli.Context) error {
				res, err := runCrawler(ctx.GlobalString("url"))
				if err != nil {
					return cli.NewExitError(fmt.Sprintf("Crawl error: %v", err), 1)
				}

				bind := ctx.String("bind")
				ui := crawler.NewWebUI(res)
				log.Infof("About to listen on %s", bind)
				log.Fatal(http.ListenAndServe(bind, ui))

				return nil
			},
		},
	}

	app.Run(os.Args)
}
