package crawler

import (
	"net/url"
	"sync"
)

// History tracks crawl visit history.
type History struct {
	mu     sync.RWMutex
	visted map[url.URL]struct{}

	URINormaliseFunc func(*url.URL) *url.URL
}

// NewHistory initialises a new instance of History with defaults.
func NewHistory() *History {
	return &History{
		visted:           make(map[url.URL]struct{}),
		URINormaliseFunc: DefaultURINormaliser,
	}
}

// Add creates a new crawl visit history entry.
func (h *History) Add(ref *url.URL) error {
	ref = &(*ref)
	ref = h.URINormaliseFunc(ref)

	h.mu.Lock()
	defer h.mu.Unlock()
	h.visted[*ref] = struct{}{}

	return nil
}

// Visited checks whether a page has been visited before.
func (h *History) Visited(ref *url.URL) bool {
	ref = &(*ref)
	ref = h.URINormaliseFunc(ref)

	h.mu.RLock()
	defer h.mu.RUnlock()
	_, ok := h.visted[*ref]

	return ok
}
