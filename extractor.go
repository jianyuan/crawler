package crawler

import (
	"bytes"
	"io"
	"net/url"

	log "github.com/Sirupsen/logrus"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

// Extractor parses web pages
type Extractor struct {
	// AtomAttrKey is a map of HTML elements to keys of a HTML attribute
	// to take the URI from.
	AtomAttrKey map[atom.Atom]string

	// URINormaliseFunc is an optional function that is applied to every URIs
	// detected on a page.
	URINormaliseFunc func(*url.URL) *url.URL
}

// DefaultAtomAttrKey defines common HTML elements and its corresponding
// HTML attribute to take the URI from.
var DefaultAtomAttrKey = map[atom.Atom]string{
	atom.A:      "href",
	atom.Img:    "src",
	atom.Link:   "href",
	atom.Script: "src",
}

// NewExtractor initialises an instance of Extractor with defaults.
func NewExtractor() *Extractor {
	return &Extractor{
		AtomAttrKey:      DefaultAtomAttrKey,
		URINormaliseFunc: DefaultURINormaliser,
	}
}

// assetKey is used as a map key to de-duplicate assets.
type assetKey struct {
	atm atom.Atom
	ref url.URL
}

// Do reads the page body and extracts the page title and enumerate a list of assets.
func (e *Extractor) Do(
	r io.Reader,
	baseURL *url.URL,
) (
	title string,
	assets AssetMap,
	err error,
) {
	z := html.NewTokenizer(r)

	rawAssets := make(map[assetKey]struct{})
	var depth int
	var currAtom *atom.Atom
	var b bytes.Buffer

	for {
		tt := z.Next()

		switch tt {
		case html.ErrorToken:
			if err := z.Err(); err != io.EOF {
				return "", nil, err
			}

			// Build asset map
			assets = make(AssetMap)
			for rawAsset := range rawAssets {
				ref := rawAsset.ref
				asset := &Asset{URL: &ref}
				assets[rawAsset.atm] = append(assets[rawAsset.atm], asset)
			}

			return title, assets, nil

		case html.TextToken:
			// Make sure we're in a <title>
			if currAtom != nil && *currAtom == atom.Title && depth > 0 {
				b.Write(z.Text())
			}

		case html.StartTagToken, html.EndTagToken:
			t := z.Token()
			currAtom = &t.DataAtom

			if t.DataAtom == atom.Title {
				if tt == html.StartTagToken {
					depth++
				} else {
					depth--
				}

				if depth == 0 {
					// Title has been extracted, flush the data now
					title = b.String()
					b.Reset()
				}
			} else if attrKey, ok := e.AtomAttrKey[t.DataAtom]; ok {
				rawURL := attrValByKey(t.Attr, attrKey)
				ref, err := url.Parse(rawURL)
				if err != nil {
					log.Errorf("[Extractor] Invalid URL: %s", err)
					continue
				}

				// Resolves a URI reference to an absolute URI reference
				if baseURL != nil {
					ref = baseURL.ResolveReference(ref)
				}

				if e.URINormaliseFunc != nil {
					ref = e.URINormaliseFunc(ref)
				}

				k := assetKey{
					atm: t.DataAtom,
					ref: *ref,
				}
				rawAssets[k] = struct{}{}
			}
		}
	}
}

// attrValByKey gets the value of HTML attribute by key.
func attrValByKey(attrs []html.Attribute, key string) string {
	for _, attr := range attrs {
		if attr.Key == key {
			return attr.Val
		}
	}
	return ""
}
