package crawler

import (
	"fmt"
	"io"
)

// ConsoleUI is console-based crawl result renderer.
type ConsoleUI struct {
	res *CrawlResult
}

// NewConsoleUI initialises a WebUI server handler with defaults.
func NewConsoleUI(res *CrawlResult) *ConsoleUI {
	return &ConsoleUI{
		res: res,
	}
}

// RenderTo renders the crawl result to w.
func (ui *ConsoleUI) RenderTo(w io.Writer) {
	fmt.Fprintf(w, " - Base URL: %s\n\n", ui.res.BaseURL)
	for _, page := range ui.res.Pages {
		fmt.Fprintf(w, " * URL: %s\n", page.URL)
		if page.Err != nil {
			fmt.Fprintf(w, "   Error: %s\n", page.Err)
		}
		if page.Status != "" {
			fmt.Fprintf(w, "   Status: %s\n", page.Status)
		}
		if page.ContentType != "" {
			fmt.Fprintf(w, "   Content Type: %s\n", page.ContentType)
		}
		if page.Title != "" {
			fmt.Fprintf(w, "   Title: %s\n", page.Title)
		}

		for a, assets := range page.Assets {
			fmt.Fprintf(w, "   <%s />:\n", a)
			for _, asset := range assets {
				fmt.Fprintf(w, "   - %s\n", asset.URL)
			}
			fmt.Fprintln(w)
		}
	}
	fmt.Fprintf(w, " - Crawling and processing time: %s\n\n", ui.res.RunTime)
}
