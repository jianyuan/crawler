package crawler

import (
	"fmt"
	"html/template"
	"io"
	"net/http"
	"net/url"
	"time"

	log "github.com/Sirupsen/logrus"
)

// DefaultTemplateLayout contains the default WebUI layout template.
var DefaultTemplateLayout = template.Must(template.New("layout").Parse(`
<!DOCTYPE html>
<html>
<head>
  <title>{{.Title}}</title>
  <style type="text/css" media="all">
  body { font-family:monospace; font-size:14px; margin: 2ex 4ex }
  p { margin: 0 0 2ex 4ex }
  </style>
</head>
<body>
  <h1>
		{{block "title" .}}
			{{.Title}}
		{{end}}
	</h1>

  {{template "body" .}}
</body>
</html>
`))

// DefaultTemplateIndex contains the default WebUI index template.
var DefaultTemplateIndex = template.Must(template.Must(DefaultTemplateLayout.Clone()).New("body").Parse(`
<h2>STATS</h2>
<p>Crawling and processing time: {{.RunTime}}</p>

<h2>SITEMAP</h2>
<ol>
  {{range .Pages}}
    <li>
      <a href="/_page?url={{.URL}}">
        {{if .URL.Path}}
          {{.URL.Path -}}
        {{else}}
          (root)
        {{end}}
        {{if .URL.RawQuery}}
          {{- "?"}}{{.URL.RawQuery -}}
        {{end}}
        {{if .URL.Fragment}}
          {{- "#"}}{{.URL.Fragment -}}
        {{end}}
      </a>
    </li>
  {{end}}
</ol>
`))

// DefaultTemplateDetails contains the default WebUI page details template.
var DefaultTemplateDetails = template.Must(template.Must(DefaultTemplateLayout.Clone()).New("body").Parse(`

{{define "title"}}
	<a href="/">{{.BaseURL}}</a>
	↦
	<a href="{{.Page.URL}}">
		{{.Page.URL.Path -}}
		{{if .Page.URL.RawQuery}}
			{{- "?"}}{{.Page.URL.RawQuery -}}
		{{end}}
		{{if .Page.URL.Fragment}}
			{{- "#"}}{{.Page.URL.Fragment -}}
		{{end}}
	</a>
{{end}}

{{if .Page.Err}}
  <h2>ERROR</h2>
  <p>{{.Page.Err}}</p>
{{end}}
{{if .Page.Status}}
  <h2>STATUS</h2>
  <p>{{.Page.Status}}</p>
{{end}}
{{if .Page.ContentType}}
  <h2>CONTENT TYPE</h2>
  <p>{{.Page.ContentType}}</p>
{{end}}
{{if .Page.Title}}
  <h2>PAGE TITLE</h2>
  <p>{{.Page.Title}}</p>
{{end}}
{{if .Page.Assets}}
	<h2>ASSETS</h2>
	{{range $atom, $assets := .Page.Assets}}
	  <h3><{{$atom}} /></h3>
	  <ol>
	    {{range $assets}}
	      <li>
					{{if .Visited}}
						<a href="/_page?url={{.URL}}">
			        {{.URL}}
			      </a>
					{{else}}
						{{.URL}}
					{{end}}
	      </li>
	    {{end}}
	  </ol>
	{{end}}
{{end}}
`))

// PageIndex holds variables for the index template.
type PageIndex struct {
	Title   string
	Pages   []*Page
	RunTime time.Duration
}

// PageDetails holds variables for the page details template.
type PageDetails struct {
	Title   string
	BaseURL *url.URL
	Page    *Page
}

// WebUI holds a server handler that renders a crawl result as a website.
type WebUI struct {
	TemplateLayout  *template.Template
	TemplateIndex   *template.Template
	TemplateDetails *template.Template

	res *CrawlResult
	mux *http.ServeMux
}

// NewWebUI initialises a WebUI server handler with defaults.
func NewWebUI(res *CrawlResult) *WebUI {
	ui := &WebUI{
		TemplateLayout:  DefaultTemplateLayout,
		TemplateIndex:   DefaultTemplateIndex,
		TemplateDetails: DefaultTemplateDetails,
		res:             res,
		mux:             http.NewServeMux(),
	}

	ui.mux.HandleFunc("/", ui.handleSitemap)
	ui.mux.HandleFunc("/_page", ui.handlePage)

	return ui
}

// ServeHTTP handles HTTP requests
func (ui *WebUI) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// Log requests
	defer func(t time.Time) {
		log.WithFields(log.Fields{
			"method": r.Method,
			"path":   r.URL.Path,
			"took":   time.Since(t),
		}).Info("Served")
	}(time.Now())

	ui.mux.ServeHTTP(w, r)
}

func (ui *WebUI) handleSitemap(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}

	renderLayout(w, ui.TemplateIndex, &PageIndex{
		Title:   ui.res.BaseURL.String(),
		Pages:   ui.res.Pages,
		RunTime: ui.res.RunTime,
	})
}

func (ui *WebUI) handlePage(w http.ResponseWriter, r *http.Request) {
	iref := r.URL.Query().Get("url")
	if iref == "" {
		http.Error(w, "URL is blank", http.StatusBadRequest)
		return
	}

	ref, err := url.Parse(iref)
	if err != nil {
		http.Error(w, fmt.Sprintf("URL is invalid: %v", err), http.StatusBadRequest)
		return
	}

	// Normalise URL
	iref = ref.String()

	for _, page := range ui.res.Pages {
		if pref := page.URL.String(); pref == iref {
			renderLayout(w, ui.TemplateDetails, &PageDetails{
				Title:   pref,
				BaseURL: ui.res.BaseURL,
				Page:    page,
			})
			return
		}
	}

	http.Error(w, fmt.Sprintf("URL not crawled: %s", iref), http.StatusNotFound)
}

func renderLayout(w io.Writer, t *template.Template, data interface{}) {
	err := t.ExecuteTemplate(w, "layout", data)
	if err != nil {
		log.Errorf("Template execute error: %v", err)
	}
}
