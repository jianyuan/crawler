package crawler

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestHistory(t *testing.T) {
	h := NewHistory()
	require.NotNil(t, h.visted)
	require.NotNil(t, h.URINormaliseFunc)

	refUnvisited, _ := url.Parse("http://example.com/new/")
	assert.False(t, h.Visited(refUnvisited))

	ref, _ := url.Parse("http://example.com/")
	refWithFragment, _ := url.Parse("http://example.com/#fragment")
	assert.NoError(t, h.Add(ref))
	assert.True(t, h.Visited(refWithFragment), "should ignore URI fragment")
}
